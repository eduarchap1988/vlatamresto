package com.vlatam.vlatmrest.Models

data class ResponseEmpM(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val emp_m: List<EmpM>
)