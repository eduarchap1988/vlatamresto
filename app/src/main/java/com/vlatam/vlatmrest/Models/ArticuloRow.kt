package com.vlatam.vlatmrest.Models

import com.google.gson.Gson

data class ArticuloRow (val id: Int, val articulo: Articulo, val cant: Int, val descripcion: String, val isEditable: Boolean){
    override fun toString(): String {
        return Gson().toJson(this)
    }
}