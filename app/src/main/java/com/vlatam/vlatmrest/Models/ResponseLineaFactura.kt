package com.vlatam.vlatmrest.Models

data class ResponseLineaFactura(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val fac_apa_lin_t: List<FacApaLinT>
)