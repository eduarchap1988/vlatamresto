package com.vlatam.vlatmrest.Models

data class EmpM(
        val id: String,
        val name: String,
        val ent: Int,
        val por_iva_gen: Int,
        val por_iva_red: Int,
        val por_iva_sup: Int,
        val por_iva_esp: Int
)