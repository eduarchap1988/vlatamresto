package com.vlatam.vlatmrest.Models

data class ResponseSalT(
        var count: Int,
        var total_count: Int,
        var api_key: String,
        var swagger: String,
        var sal_t: List<SalT>
)