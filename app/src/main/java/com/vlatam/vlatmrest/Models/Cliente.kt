package com.vlatam.vlatmrest.Models

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatmrest.Helper.DBHelper

@DatabaseTable(tableName = "cliente")
data class Cliente(
        @DatabaseField(generatedId = false, id = true)
        val id: Int? = null,

        @DatabaseField
        val nom_fis: String = "",

        @DatabaseField
        val nom_com: String = "",

        @DatabaseField
        val cif: String = "",

        @DatabaseField
        val dir: String = "",

        @DatabaseField
        val cmr: Int? = null
)

class ClienteDao{

    companion object {
        lateinit var dao: Dao<Cliente, Int>
    }

    init {
        dao = DBHelper.getClienteDao()
    }

    fun add(table: Cliente) = dao.createOrUpdate(table)

    fun update(table: Cliente) = dao.update(table)

    fun delete(table: Cliente) = dao.delete(table)

    fun queryForAll() = dao.queryForAll()

    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }

}