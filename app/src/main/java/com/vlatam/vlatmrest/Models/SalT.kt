package com.vlatam.vlatmrest.Models

data class SalT(
        var id: Int,
        var name: String,
        var obj: String
)