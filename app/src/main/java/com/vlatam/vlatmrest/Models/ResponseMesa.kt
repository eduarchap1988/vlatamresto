package com.vlatam.vlatmrest.Models


data class ResponseMesa(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val mes_t: List<Mesa>
)