package com.vlatam.vlatmrest.Models

import com.google.gson.Gson

data class MesaCliente (val listMesa: MutableList<Mesa>, val listCliente: MutableList<Cliente>){
    override fun toString(): String {
        return Gson().toJson(this)
    }
}