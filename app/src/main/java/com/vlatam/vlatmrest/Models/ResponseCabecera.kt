package com.vlatam.vlatmrest.Models

data class ResponseCabecera(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val fac_apa_t: List<FacApaT>
)