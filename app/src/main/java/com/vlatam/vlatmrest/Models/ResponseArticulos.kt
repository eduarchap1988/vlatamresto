package com.vlatam.vlatmrest.Models


data class ResponseArticulos(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val art_m: List<Articulo>
)


