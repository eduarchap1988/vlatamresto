package com.vlatam.vlatmrest.Models

data class FacApaT(
        val id: Int,
        val emp: String,
        val emp_div: String,
        val name: String,
        val fch: String,
        val hor: String,
        val clt: Int,
        val obs: String,
        val trm_tpv: Int,
        val tot: Double,
        val dep_tpv: Int,
        val mes_t: Int,
        val alt_tim: String,
        val mod_tim: String,
        val ______personalizacion______: String,
        val api_key: String,
        val ser: Int,
        val por_dto: Int,
        val alt_usr: Int,
        val trm_ptr: String,
        val txt: String,
        val nro_tkt: Int,
        val en_edt: Boolean,
        val api_key_edt: String,
        val dep_tpv_edt: Int
)