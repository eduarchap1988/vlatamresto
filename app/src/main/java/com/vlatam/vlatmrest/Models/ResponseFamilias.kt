package com.vlatam.vlatmrest.Models


data class ResponseFamilias(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val fam_m: List<Familia>
)