package com.vlatam.vlatmrest.Activities.Adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatmrest.Models.Articulo
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.item_articulos.view.*


class AdapterArticulosFamilia (val items : MutableList<Articulo>, val context: Context, val listener: (Articulo) -> Unit): RecyclerView.Adapter<AdapterArticulosFamilia.ViewHolder>() {
    private val items_aux= items

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_articulos, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun filter(texto: String){
        if(!texto.isEmpty()) {
            var query_articulo = ArrayList<Articulo>()

            for (art in items_aux) {
                if (art.name.toLowerCase().contains(texto.toLowerCase()))
                    query_articulo.add(art)
            }

            if (query_articulo.size > 0) {
                items.clear()
                items.addAll(query_articulo)
            }
        }else{
            items.clear()
            items.addAll(items_aux)
        }
        notifyDataSetChanged()

    }


    class ViewHolder (val view: View)  : RecyclerView.ViewHolder(view) {
       fun bind(item: Articulo, listener: (Articulo) -> Unit){
            if(!item.img.isEmpty()){
                val decodedString = Base64.decode(item.img, Base64.DEFAULT)
                val decodedByte: Bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                view.img_articulo.setImageBitmap(decodedByte)
            }

           view.articulo.text= item.name
           view.descripcion.text= item.ref
           view.price.text= "$ "+item.pvp.toString()

            view.setOnClickListener { listener(item) }
        }
    }
}