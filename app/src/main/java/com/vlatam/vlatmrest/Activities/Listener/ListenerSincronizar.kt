package com.vlatam.vlatmrest.Activities.Listener

import com.vlatam.vlatmrest.Models.Pedido

interface ListenerSincronizar {
    fun sincronizarPedido(pedido: Pedido)
}