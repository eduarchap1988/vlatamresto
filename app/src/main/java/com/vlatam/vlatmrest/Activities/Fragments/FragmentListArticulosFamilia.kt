package com.vlatam.vlatmrest.Activities.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatmrest.Activities.Adapters.AdapterArticulosFamilia
import com.vlatam.vlatmrest.Models.Articulo
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.fragment_list_familias.*

class FragmentListArticulosFamilia : Fragment(){
    lateinit var list : MutableList<Articulo>
    lateinit var listener : (Articulo) -> Unit
    lateinit var adapter: AdapterArticulosFamilia

    companion object {
        fun newInstance(articuloList: MutableList<Articulo>, listener: (Articulo) -> Unit): FragmentListArticulosFamilia {
            var fragment= FragmentListArticulosFamilia()
            fragment.list= articuloList
            fragment.listener= listener
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_list_familias, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter= AdapterArticulosFamilia(list, context!!, listener)
        recycler.layoutManager= LinearLayoutManager(context)
        recycler.adapter = adapter
    }

    fun onSearchArticulo(texto: String) {
        adapter.filter(texto)
    }
}