package com.vlatam.vlatmrest.Activities.Adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatmrest.Models.ArticuloRow
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.item_articulos_pedido.view.*


class AdapterArticulos (val items : MutableList<ArticuloRow>, val context: Context, val listener: (ArticuloRow) -> Unit): RecyclerView.Adapter<AdapterArticulos.ViewHolder>() {
    val items_aux= items

    val lowerCaseFormatter: (String) -> String = { it.toLowerCase() }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_articulos_pedido, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun filter(texto: String){
        if(!texto.isEmpty()) {
            var query_articulo = ArrayList<ArticuloRow>()

            for (art in items_aux) {
                if (art.articulo.name.toLowerCase().contains(texto.toLowerCase()))
                    query_articulo.add(art)
            }

            if (query_articulo.size > 0) {
                items.clear()
                items.addAll(query_articulo)
            }
        }else{
            items.clear()
            items.addAll(items_aux)
        }
        notifyDataSetChanged()

    }

    class ViewHolder (val view: View)  : RecyclerView.ViewHolder(view) {
       fun bind(item: ArticuloRow, listener: (ArticuloRow) -> Unit){
           if(!item.articulo.img.isEmpty()){
               val decodedString = Base64.decode(item.articulo.img, Base64.DEFAULT)
               val decodedByte: Bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
               view.img_articulo.setImageBitmap(decodedByte)
           }

           view.articulo.text= item.articulo.name
           view.descripcion.text= item.descripcion
           view.price.text= "precio: $ ${item.articulo.pvp.toString()}"
           view.cantidad.text= "cantidad: ${item.cant.toString()}"

           view.setOnClickListener { listener(item) }
        }
    }
}