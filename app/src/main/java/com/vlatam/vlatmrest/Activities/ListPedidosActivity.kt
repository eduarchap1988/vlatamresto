package com.vlatam.vlatmrest.Activities

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.vlatam.vlatmrest.Activities.Adapters.AdapterPedidosSinMesa
import com.vlatam.vlatmrest.Activities.Dialogos.DialogoConfirmarEditar
import com.vlatam.vlatmrest.Helper.Status
import com.vlatam.vlatmrest.Models.Pedido
import com.vlatam.vlatmrest.R
import com.vlatam.vlatmrest.ViewModels.PedidosViewModel
import kotlinx.android.synthetic.main.activity_list_pedidos.*
import org.koin.android.viewmodel.ext.android.viewModel
import android.app.Activity
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class ListPedidosActivity : AppCompatActivity() {

    private val pedidosViewModel: PedidosViewModel by viewModel()

    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, ListPedidosActivity::class.java)
            //intent.putExtra(INTENT_USER_ID, user.id)
            return intent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_pedidos)

        setupView()
        setupPedido()
        pedidosViewModel.getListPedidos()
    }
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))

    }

    private fun setupPedido() {
        pedidosViewModel.pedidos.observe(this, Observer {


            when (it!!.status) {
                Status.SUCCESS -> {
                    refreshAdapter(it.data!!)
                }
                Status.LOADING -> {
                    Log.e("TAG", "pedidos loading")
                    showProgress()
                }
                Status.ERROR -> {
                    Log.e("TAG", "pedidos error")
                    hideProgress()
                    showToast(it.message!!)
                }
            }


        })


        pedidosViewModel.pedidos_con_mesa.observe(this, Observer {

            when (it!!.status) {
                Status.SUCCESS -> {
                    hideProgress()
                    setupAdapterConMesa(it.data!!)
                }
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    showToast(it.message!!)
                }
            }


        })



        pedidosViewModel._pedido.observe(this, Observer {

            when (it!!.status) {
                Status.SUCCESS -> {
                    //adapterPedidos.update(it.data!!, POS)
                }
                Status.LOADING -> {

                }
                Status.ERROR -> {
                    val returnIntent = Intent()
                    setResult(Activity.RESULT_OK, returnIntent)
                    finish()
                }
            }


        })
    }

    private fun setupAdapterConMesa(data: MutableList<Pedido>) {

        var adapterPedidos= AdapterPedidosSinMesa(data, this, 0 ) { pedido->
            if(pedido.status.equals("Sincronizado")){
                var dialogo: DialogoConfirmarEditar = DialogoConfirmarEditar.newInstance { valor ->
                    if (valor) {
                        pedidosViewModel.editarPedido(pedido)
                    }
                }
                dialogo.show(supportFragmentManager, "my_fragment")

            }else{

            }

        }
        recycler_con_mesa.adapter = adapterPedidos
    }

    private fun refreshAdapter(listPedidos: MutableList<Pedido>) {
        Log.e("TAG", "pedidos succes ${listPedidos}")

        var adapterPedidos= AdapterPedidosSinMesa(listPedidos, this, 0 ) { pedido->

            if(pedido.status.equals("Sincronizado")){
                var dialogo: DialogoConfirmarEditar = DialogoConfirmarEditar.newInstance { valor ->
                    if (valor) {
                        pedidosViewModel.editarPedido(pedido)
                    }
                }
                dialogo.show(supportFragmentManager, "my_fragment")
            }

        }
        recycler.adapter = adapterPedidos

    }


    fun showProgress() {
        layout_cabecera.visibility= View.GONE
        animation_view2.visibility= View.VISIBLE
    }

    fun hideProgress() {
        layout_cabecera.visibility= View.VISIBLE
        animation_view2.visibility= View.GONE
    }

    private fun showToast(message: String) {
        val mSnackBar = Snackbar.make(layout, message, Snackbar.LENGTH_LONG)
        val mainTextView = mSnackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mainTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        } else {
            mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        }
        mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        mainTextView.setTextColor(resources.getColor(R.color.colorPrimary))
        mSnackBar.show()
    }

    private fun setupView() {
        toolbar.title="Lista de Pedidos"
        toolbar.setNavigationIcon(R.drawable.arrow_left)
        toolbar.setNavigationOnClickListener {onBackPressed()}

        recycler.layoutManager= LinearLayoutManager(this)
        recycler_con_mesa.layoutManager= LinearLayoutManager(this)

    }
}
