package com.vlatam.vlatmrest.Activities.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatmrest.Activities.Adapters.AdapterArticulos
import com.vlatam.vlatmrest.Models.ArticuloRow
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.fragment_detalles_pedido.*

class FragmentDetallePedido: Fragment() {
    private var list = ArrayList<ArticuloRow>()
    lateinit var listener : (ArticuloRow) -> Unit

    companion object {
        fun newInstance( articulosList: MutableList<ArticuloRow>, listener: (ArticuloRow) -> Unit): FragmentDetallePedido {
            var fragment= FragmentDetallePedido()
            fragment.list.addAll(articulosList)
            fragment.listener= listener
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_detalles_pedido, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            recycler.layoutManager= GridLayoutManager(context, 2)
            recycler.adapter = AdapterArticulos(list, context!!, listener)
        }catch (e: Exception){

        }

    }
}