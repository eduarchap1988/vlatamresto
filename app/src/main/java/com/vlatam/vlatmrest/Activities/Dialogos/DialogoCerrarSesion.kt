package com.vlatam.vlatmrest.Activities.Dialogos

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.Window
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.dialogo_sali.*


class DialogoCerrarSesion: DialogFragment() {
    private lateinit var listener: (Boolean) -> Unit

    companion object {
        fun newInstance(listener: (Boolean) -> Unit): DialogoCerrarSesion {
            var dialogo= DialogoCerrarSesion()
            dialogo.listener= listener
            return dialogo
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogo = super.onCreateDialog(savedInstanceState)
        dialogo.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialogo.setContentView(R.layout.dialogo_sali)
        dialogo.setCancelable(true)

        initView(dialogo)

        return dialogo
    }

    private fun initView(dialogo: Dialog) {
        dialogo.titulo.text= getString(R.string.cerrar_sesion)
        dialogo.mensaje.text= getString(R.string.mensaje_cerrar)
        dialogo.aceptar.text= getString(R.string.si)
        dialogo.cancelar.text= getString(R.string.no)


        dialogo.aceptar.setOnClickListener{
            listener(true)
            dialogo.dismiss()
        }

        dialogo.cancelar.setOnClickListener{
            dialogo.dismiss()
        }


    }

}