package com.vlatam.vlatmrest.Activities.Listener

import com.vlatam.vlatmrest.Models.Cliente
import com.vlatam.vlatmrest.Models.Mesa

interface ListenerEnviarPedido {
    fun enviar(des: String, sin_mesa: Boolean, sin_cliente: Boolean, mesa: Mesa, comensales: Int, cliente: Cliente)
}