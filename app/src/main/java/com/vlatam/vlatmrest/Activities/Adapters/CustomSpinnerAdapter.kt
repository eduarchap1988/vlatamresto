package com.vlatam.vlatmrest.Activities.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.vlatam.vlatmrest.Models.Mesa
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.view_simple_spinner_custom.view.*

class CustomSpinnerAdapter (val context: Context, var listItemsTxt: MutableList<Mesa>) : BaseAdapter() {


    val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View

        if (convertView == null) {
            view = mInflater.inflate(R.layout.view_simple_spinner_custom, parent, false)
        } else {
            view = convertView
        }

        if(listItemsTxt[position].est==0){
            view.estado.setImageResource(android.R.color.holo_green_dark)
        }else
            if(listItemsTxt[position].est==1){
                view.estado.setImageResource(android.R.color.holo_red_dark)
            }else{
                view.estado.setImageResource(android.R.color.holo_orange_dark)
            }
        //view.label.text = "Salón ${listItemsTxt[position].sal} - Mesa ${listItemsTxt[position].num}"
        view.label.text = listItemsTxt[position].name
        return view
    }

    override fun getItem(position: Int): Mesa {
        return listItemsTxt[position]
    }

    override fun getItemId(position: Int): Long {
        return listItemsTxt[position].id!!.toLong()
    }

    override fun getCount(): Int {
        return listItemsTxt.size
    }

}