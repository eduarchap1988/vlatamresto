package com.vlatam.vlatmrest.Activities.Listener

import com.vlatam.vlatmrest.Models.ArticuloRow

interface ListenerArticulos {
    fun eliminar(articuloRow: ArticuloRow)
    fun actualizar(articuloRow: ArticuloRow)
}