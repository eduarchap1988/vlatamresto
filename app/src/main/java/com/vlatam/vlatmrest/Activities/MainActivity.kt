package com.vlatam.vlatmrest.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.vlatam.vlatmrest.Activities.Adapters.AdapterPedidos
import com.vlatam.vlatmrest.Activities.Dialogos.*
import com.vlatam.vlatmrest.Activities.Fragments.FragmentDetallePedido
import com.vlatam.vlatmrest.Activities.Fragments.FragmentListArticulosFamilia
import com.vlatam.vlatmrest.Activities.Fragments.FragmentListFamilias
import com.vlatam.vlatmrest.Activities.Listener.ListenerArticulos
import com.vlatam.vlatmrest.Activities.Listener.ListenerEnviarPedido
import com.vlatam.vlatmrest.Activities.Listener.ListenerSincronizar
import com.vlatam.vlatmrest.Helper.Status
import com.vlatam.vlatmrest.Models.*
import com.vlatam.vlatmrest.R
import com.vlatam.vlatmrest.ViewModels.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.navigation_view.*
import org.koin.android.viewmodel.ext.android.viewModel
import android.app.Activity
import android.R.attr.data
import android.app.ProgressDialog
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.vlatam.vlatmrest.VlatamRestoApp
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class MainActivity : AppCompatActivity() , ListenerSincronizar, ListenerArticulos, ListenerEnviarPedido {


    private lateinit var mSearch: MenuItem
    private lateinit var mSearchView: SearchView
    private lateinit var nuevoPedido: MenuItem
    private var estado: Int= 1 //1 detalle de pedido, 2 familia, 3 detalle de una familia

    private val mainViewModel: MainViewModel by viewModel()

    private lateinit var adapterPedidos: AdapterPedidos
    private lateinit var adapterPedidos_s: AdapterPedidos

    private lateinit var listPedidos: MutableList<Pedido>
    private lateinit var listPedidos_s: MutableList<Pedido>

    private var POS: Int=0
    private var POS_SINCRONIZADOS: Int=-1


    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            //intent.putExtra(INTENT_USER_ID, user.id)
            return intent
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        setupView()
        setupDataUser()
        setupPedidos()
        setupFamilias()
        setupArticulos()
        setupArticulosPedido()
        setupMesas()
        permisos()
        //mainViewModel.enviarToken()
    }
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))

    }

    private fun permisos() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.CAMERA),
                    2)
        }
    }

    override fun onResume() {
        super.onResume()
        mainViewModel.checkDatosInicio()
    }


    private fun setupMesas() {

        mainViewModel.mesas.observe(this, Observer {
            when (it!!.status) {
                Status.SUCCESS -> {
                    hideProgress()
                    showDialogoSincronizar(it.data!!.listMesa, it.data!!.listCliente)
                }
                Status.LOADING -> {
                    Log.e("TAG", "pedidos loading")
                    showProgress()
                }
                Status.ERROR -> {
                    showToast(it.message!!)
                    Log.e("TAG", "pedidos error")
                    hideProgress()
                }
            }

        })


    }

    private fun showToast(message: String) {
        val mSnackBar = Snackbar.make(drawer_layout, message, Snackbar.LENGTH_LONG)
        val mainTextView = mSnackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mainTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        } else {
            mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        }
        mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        mainTextView.setTextColor(resources.getColor(R.color.colorPrimary))
        mSnackBar.show()
    }

    fun showDialogoSincronizar(listMesa: MutableList<Mesa>, listCliente: MutableList<Cliente>) {
        var dialogo : DialogoSeleccionMesa = DialogoSeleccionMesa.newInstance(listMesa, listCliente, this)
        dialogo.show(supportFragmentManager,"my_fragment")
    }

    override fun enviar(des: String, sin_mesa: Boolean, sin_cliente: Boolean, mesa: Mesa, comensales: Int, cliente: Cliente) {
        mainViewModel.enviar(des, sin_mesa, sin_cliente, mesa, comensales, cliente)

    }


    private fun setupArticulosPedido() {


        mainViewModel.articulos_pedido.observe(this, Observer {
            when (it!!.status) {
                Status.SUCCESS -> {
                    hideProgress()
                    showListArticulos(it.data!!)
                }
                Status.LOADING -> {
                    Log.e("TAG", "pedidos loading")
                    showProgress()
                }
                Status.ERROR -> {
                    Log.e("TAG", "pedidos error")
                    hideProgress()
                    showToast(it.message!!)
                }
            }

        })

    }

    @SuppressLint("RestrictedApi")
    fun showListArticulos(articulos: MutableList<ArticuloRow>) {
        Log.e("TAG", "ejecutando showListArticulos")

        estado = 1


        var fragment_articulos = FragmentDetallePedido.newInstance(articulos) { articulo ->

            var dialogo: DialogoDetalleArticulos = DialogoDetalleArticulos.newInstance(articulo, this)
            dialogo.show(supportFragmentManager, "my_fragment")

        }

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment, fragment_articulos, "fragmentpedido")
                .commit()


        try{
            mSearch.isVisible = false
            fab.visibility = View.VISIBLE

        }catch (e: Exception){

        }

    }


    override fun eliminar(articuloRow: ArticuloRow) {
        var dialogo : DialogoConfirmarEliminar = DialogoConfirmarEliminar.newInstance { valor->
            mainViewModel.deleteArticulo(articuloRow)
        }
        dialogo.show(supportFragmentManager,"my_fragment")
    }

    override fun actualizar(articuloRow: ArticuloRow) {
        mainViewModel.updateArticulo(articuloRow)
    }

    private fun setupArticulos() {

        mainViewModel.articulos.observe(this, Observer {
            when (it!!.status) {
                Status.SUCCESS -> {
                    hideProgressPedidos()
                    showArticulos(it.data!!)
                }
                Status.LOADING -> {
                    Log.e("TAG", "pedidos loading")
                    showProgress()
                }
                Status.ERROR -> {
                    Log.e("TAG", "pedidos error")
                    hideProgress()
                    showToast(it.message!!)
                }
            }

        })


    }

    @SuppressLint("RestrictedApi")
    private fun showArticulos(listArticulo: MutableList<Articulo>) {

        estado= 3
        var fragment_articulos_familia= FragmentListArticulosFamilia.newInstance(listArticulo){ articulo ->
            showDialogoArticulo(articulo)
        }

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment, fragment_articulos_familia, "fragmentarticulosfamilias")
                .commit()

        try {
            fab.visibility= View.GONE
        }catch (e: Exception){
        }

    }

    fun showDialogoArticulo(articulo: Articulo) {

        var dialogo : DialogoDetalleArticulos = DialogoDetalleArticulos.newInstance(articulo){ articuloRow ->
            mainViewModel.addArticulo(articuloRow)
        }
        dialogo.show(supportFragmentManager,"my_fragment")

    }


    private fun setupFamilias() {

        mainViewModel.familias.observe(this, Observer {
            when (it!!.status) {
                Status.SUCCESS -> {
                    hideProgressPedidos()
                    showFamilias(it.data!!)
                }
                Status.LOADING -> {
                    Log.e("TAG", "pedidos loading")
                    showProgress()
                }
                Status.ERROR -> {
                    Log.e("TAG", "pedidos error")
                    hideProgress()
                    showToast(it.message!!)
                }
            }

        })


    }

    @SuppressLint("RestrictedApi")
    private fun showFamilias(listFamilias: MutableList<Familia>) {
        estado= 2
        var fragment_familias= FragmentListFamilias.newInstance(listFamilias){ familia ->
            mainViewModel.selectedFamilia(familia)
        }

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment, fragment_familias, "fragmentfamilias")
                .commit()

        mSearch.isVisible = true

        fab.visibility= View.GONE
    }


    private var entrar: Boolean= true

    private fun setupPedidos() {
        mainViewModel.pedidos.observe(this, Observer {

            when (it!!.status) {
                Status.SUCCESS -> {
                    hideProgress()
                    hideProgressPedidos()
                    listPedidos=it.data!!

                    if(entrar){
                        entrar=false
                        POS=-1
                        POS_SINCRONIZADOS=-1
                        refreshAdapter()
                    }else{
                        POS=0
                        POS_SINCRONIZADOS=-1
                        refreshAdapter()
                        mainViewModel.selectedPedido(listPedidos[POS])
                    }

                }
                Status.LOADING -> {
                    Log.e("TAG", "pedidos loading")
                    showProgress()
                    showProgressPedidos()
                }
                Status.ERROR -> {
                    Log.e("TAG", "pedidos error")
                    hideProgress()
                    hideProgressPedidos()
                    showToast(it.message!!)
                }
            }

        })

        mainViewModel.pedidos_sincronizados.observe(this, Observer {

            when (it!!.status) {
                Status.SUCCESS -> {
                    listPedidos_s = it.data!!
                    refreshAdapterSincronizados()
                }
                Status.LOADING -> {
                }
                Status.ERROR -> {
                }
            }

        })



        mainViewModel.pedido.observe(this, Observer {

            when (it!!.status) {
                Status.SUCCESS -> {
                    adapterPedidos.update(it.data!!, POS)
                    adapterPedidos_s.update(it.data!!, POS)
                }
                Status.LOADING -> {
                }
                Status.ERROR -> {
                }
            }
        })




    }

    fun showProgressPedidos() {
        animation_view.visibility = View.VISIBLE
        switch_pedido.visibility = View.GONE
        nuevo_pedido.visibility = View.GONE
        recycler_lista_pedidos.visibility = View.GONE
    }

    fun hideProgressPedidos() {
        animation_view.visibility= View.GONE
        switch_pedido.visibility = View.GONE
        nuevo_pedido.visibility = View.VISIBLE
        recycler_lista_pedidos.visibility= View.VISIBLE
    }


    fun showProgress() {
        animation_view2.visibility= View.VISIBLE
    }

    fun hideProgress() {
        animation_view2.visibility= View.GONE
    }


    private fun refreshAdapter() {
        Log.e("TAG", "pedidos succes ${listPedidos}")

        adapterPedidos= AdapterPedidos(listPedidos, this, POS, this) {pedido->

            drawer_layout.closeDrawer(GravityCompat.START)

            POS= adapterPedidos.getPos(pedido)
            POS_SINCRONIZADOS=-1
            mainViewModel.selectedPedido(pedido)

            refreshAdapter()
            refreshAdapterSincronizados()
        }
        recycler_lista_pedidos.adapter = adapterPedidos


    }


    private fun refreshAdapterSincronizados() {

        adapterPedidos_s= AdapterPedidos(listPedidos_s, this, POS_SINCRONIZADOS, this) {pedido->
            drawer_layout.closeDrawer(GravityCompat.START)
            POS= -1
            POS_SINCRONIZADOS=adapterPedidos_s.getPos(pedido)
            mainViewModel.selectedPedido(pedido)
            refreshAdapter()
            refreshAdapterSincronizados()
        }
        recycler_lista_sincronizados.adapter = adapterPedidos_s
    }

    override fun sincronizarPedido(pedido: Pedido) {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        if(pedido.total != 0.0){
            if(pedido.is_sincronizado){
                var dialogo = DialogoConfirmarCerrarPedido.newInstance { valor->
                    if(valor) {
                        mainViewModel.sincronizar()
                    }
                }
                dialogo.show(supportFragmentManager,"my_fragment")
            }else
            mainViewModel.sincronizar()
        }else{
            showToast("Imposible sincronizar pedido")
        }
    }

    private fun setupDataUser() {

        mainViewModel.user_data.observe(this, Observer{
            when (it!!.status) {
                Status.SUCCESS -> {

                    if(!it.data!!["img"]!!.isEmpty()){
                        val decodedString = Base64.decode(it.data!!["img"]!!, Base64.DEFAULT)
                        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                        profile_image.setImageBitmap(decodedByte)
                    }

                    if(!it.data!!["nom_com"]!!.isEmpty())
                        user_name.text=it.data!!["nom_com"]!!

                }
                Status.ERROR -> {
                    val intent = LoginActivity.newIntent(this)
                    //intent.putExtra("nombre", )
                    //intent.putExtra("img", )
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                }
                Status.LOADING -> {
                }
            }
        })

    }


    @SuppressLint("RestrictedApi")
    private fun setupView() {

        toolbar.title=" "
        toolbar.setNavigationIcon(R.drawable.menu)
        toolbar.setNavigationOnClickListener {drawer_layout.openDrawer(GravityCompat.START)}
        fab.visibility= View.GONE

        recycler_lista_pedidos.layoutManager= LinearLayoutManager(this)
        recycler_lista_sincronizados.layoutManager= LinearLayoutManager(this)


        fab.setOnClickListener {
            Log.e("TAG", "CLICK FAB")
            mainViewModel.mostrarFamilias()
        }

        nuevo_pedido.setOnClickListener {
            Log.e("TAG", "CLICK NUEVO PEDIDO")
            mainViewModel.nuevoPedido()
        }

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        mSearch = menu!!.findItem(R.id.action_search)
        mSearchView = mSearch.actionView as SearchView
        mSearch.isVisible = false

        nuevoPedido = menu!!.findItem(R.id.action_add)

        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(texto: String): Boolean {

                return false
            }

            override fun onQueryTextChange(texto: String): Boolean {
                when(estado){
                    1->{

                    }
                    2->{
                        mainViewModel.onSearchArticulo(texto)
                    }
                    3->{
                        mainViewModel.onSearchArticulo(texto)
                    }
                }
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.action_add->{
                var dialogo = DialogoNuevoPedido.newInstance { valor->
                    if(valor) {
                        mainViewModel.nuevoPedido()
                    }
                }
                dialogo.show(supportFragmentManager,"my_fragment")
            }

            R.id.action_gestion_pedido->{
                startActivityForResult(ListPedidosActivity.newIntent(this), 112)
            }

            R.id.action_update->{
                mainViewModel.getTablasPrincipales()
            }

            R.id.action_logout->{
                var dialogo = DialogoCerrarSesion.newInstance { valor->
                    if(valor)
                        mainViewModel.logout()
                }

                dialogo.show(supportFragmentManager,"cerrar")
            }

            R.id.action_qr->{
                val intent = Intent(this, ScannerActivity::class.java)
                startActivityForResult(intent, 132)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 112) {
            if (resultCode == Activity.RESULT_OK) {
                mainViewModel.checkDatosInicio()
            }else
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }else
            if (requestCode == 132) {
                if(resultCode == Activity.RESULT_OK){
                    var result= data!!.getStringExtra("data")

                    Log.e("TAG", "resilt OK $result")
                    if(!result.isEmpty()) {
                        showProgress()
                        mainViewModel.crearPedido(result)
                    }

                }
            }


    }

    override fun onBackPressed() {

        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            if (!mSearchView.isIconified) {
                mSearchView.isIconified = true
                return
            }

            when(estado){
                2->{
                    if(POS!=-1)
                        mainViewModel.selectedPedido(listPedidos[POS])
                    else
                        if(POS_SINCRONIZADOS!=-1)
                            mainViewModel.selectedPedido(listPedidos_s[POS_SINCRONIZADOS])
                    return
                }
                3->{
                    mainViewModel.mostrarFamilias()
                    return
                }
                1->{
                    var dialogo : DialogoSalir = DialogoSalir.newInstance { valor->
                        if(valor)
                            super.onBackPressed()
                    }
                    dialogo.show(supportFragmentManager,"my_fragment")
                }
            }



        }
    }
}
