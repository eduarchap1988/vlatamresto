package com.vlatam.vlatmrest.Activities.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatmrest.Models.Pedido
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.item_pedido.view.*

class AdapterPedidosSinMesa (val items: MutableList<Pedido>, val context: Context,
                             var pos: Int,
                             val listener: (Pedido) -> Unit): RecyclerView.Adapter<AdapterPedidosSinMesa.ViewHolder>() {
    var cont: Int=0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        //Log.e("TAG", "position $p1")
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_pedido_selected, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun getPos(pedido: Pedido): Int {
        var p: Int= 0

        for(ped in items){
            if(ped.equals(pedido)){
                pos=p
                return p
            }
            p++
        }
        return p
    }

    fun getPedido(pos: Int): Pedido {
        return  items[pos]
    }

    fun cambiarPos(pos: Int){
        this.pos= pos
        notifyDataSetChanged()
    }

    fun update(pedido: Pedido, pos_1: Int) {
        for((p, ped) in items.withIndex()){
            if(ped.equals(pedido)){
                ped.total= pedido.total
            }
        }
        this.pos= pos_1
        notifyDataSetChanged()
    }


    class ViewHolder (val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: Pedido, listener: (Pedido) -> Unit){


            view.status_value.text= item.status

            if(item.hora.isEmpty()){
                view.pedido_title.text= "Salón ${item.salon} - Mesa ${item.mesa}"
            }else
                view.pedido_title.text= item.hora


            view.total_monto.text= item.total.toString()

            view.btn_sincronizar.text= "  "

            /*view.btn_sincronizar.setOnClickListener{
                listener(item)
            }*/
            view.setOnClickListener {
                listener(item)
            }
        }

    }
}