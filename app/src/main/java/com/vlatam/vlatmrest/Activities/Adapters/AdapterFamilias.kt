package com.vlatam.vlatmrest.Activities.Adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatmrest.Models.Articulo
import com.vlatam.vlatmrest.Models.Familia
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.item_familia.view.*

class AdapterFamilias (val items : MutableList<Familia>, val context: Context, val listener: (Familia) -> Unit): RecyclerView.Adapter<AdapterFamilias.ViewHolder>() {
    val items_aux= items

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_familia, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }


    fun filter(texto: String){
        var query_familia = ArrayList<Familia>()

        for (fam in items_aux) {
            if (fam.name.toLowerCase().contains(texto.toLowerCase()))
                query_familia.add(fam)
        }

        if (query_familia.size > 0) {
            items.clear()
            items.addAll(query_familia)
        }
        notifyDataSetChanged()
    }


    class ViewHolder (val view: View): RecyclerView.ViewHolder(view) {

        fun bind(item: Familia, listener: (Familia) -> Unit){
            if(!item.img!!.isEmpty()) {
                val decodedString = Base64.decode(item.img, Base64.DEFAULT)
                val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                view.img_articulo.setImageBitmap(decodedByte)
            }

            view.pedido_title.text= item.name
            view.setOnClickListener { listener(item) }
        }

    }
}