package com.vlatam.vlatmrest.Activities

import android.arch.lifecycle.Observer
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.activity_login.*
import android.content.Intent
import android.view.Gravity
import android.os.Build
import android.widget.TextView
import android.support.design.widget.Snackbar
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.vlatam.vlatmrest.ViewModels.LoginViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.net.MalformedURLException
import java.net.URISyntaxException
import java.net.URL


class LoginActivity : AppCompatActivity(){

    private val loginViewModel: LoginViewModel by viewModel()

    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, LoginActivity::class.java)
            //intent.putExtra(INTENT_USER_ID, user.id)
            return intent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        setupSnackBar()
        setupView()
        setupActivityIncio()
    }
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))

    }


    private fun setupActivityIncio() {

        loginViewModel.user.observe(this, Observer<HashMap<String, String>> { it ->
            val intent = MainActivity.newIntent(this)
            //intent.putExtra("nombre", it!!["nom_com"])
            //intent.putExtra("img", it!!["img"])
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        })

    }

    private fun setupSnackBar() {


        loginViewModel.message.observe(this, Observer<String> { it ->
            ocultarTeclado()
            btn_iniciar.text = "INICIAR"
            showMssage(it!!)
            hideProgress()
        })

    }


    fun showMssage(texto: String) {
        val mSnackBar = Snackbar.make(constraintLogin, texto, Snackbar.LENGTH_LONG)
        val mainTextView = mSnackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mainTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        } else {
            mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        }
        mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        mainTextView.setTextColor(resources.getColor(R.color.colorPrimary))
        mSnackBar.show()

    }


    private fun setupView() {
        btn_iniciar.setOnClickListener {

            if(!edit_url.text.contains("http")){
                edit_url.setText("http://"+edit_url.text)
            }

            if(edit_apikey.text.toString().isEmpty() || edit_apikey.text.toString().isEmpty() )
                showMessage(R.string.error_campo)
            else
            if(validate(edit_url.text.toString())){
                showProgress()
                loginViewModel.login(edit_apikey.text.toString(), edit_apikey.text.toString(), edit_url.text.toString())
            }else
                showMessage(R.string.error_login)
        }
    }

    private fun validate(url: String): Boolean {
        /*validación de url*/
        try {
            URL(url).toURI()
            return true
        } catch (exception: URISyntaxException) {
            return false
        } catch (exception: MalformedURLException) {
            return false
        }
    }

    fun showProgress() {
        btn_iniciar.text = "INICIANDO..."
    }

    fun hideProgress() {
        btn_iniciar.text = "INICIAR"
    }

    fun showMessage(message: Int) {
        ocultarTeclado()
        val mSnackBar = Snackbar.make(constraintLogin, message, Snackbar.LENGTH_LONG)
        val mainTextView = mSnackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mainTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        } else {
            mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        }
        mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        mainTextView.setTextColor(resources.getColor(R.color.colorPrimary))
        mSnackBar.show()
    }

    fun ocultarTeclado() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(constraintLogin.windowToken, 0)
    }


}
