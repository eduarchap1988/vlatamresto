package com.vlatam.vlatmrest.ViewModels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.google.gson.Gson
import com.vlatam.vlatmrest.Helper.Resource
import com.vlatam.vlatmrest.Models.*
import com.vlatam.vlatmrest.R
import com.vlatam.vlatmrest.Servicios.PrefrencesManager
import com.vlatam.vlatmrest.VlatamRestoApp
import okhttp3.*
import org.json.JSONObject
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import java.io.IOException
import java.util.ArrayList

class PedidosViewModel constructor (val sesion: PrefrencesManager): ViewModel() {
    var pedidos = MutableLiveData<Resource<MutableList<Pedido>>> () /**  lista de pedidos del menu drawer **/
    var pedidos_con_mesa = MutableLiveData<Resource<MutableList<Pedido>>> () /**  lista de pedidos del menu drawer **/
    var _pedido = MutableLiveData<Resource<Pedido>> () /**  lista de pedidos del menu drawer **/

    val daoPedidos = PedidoDao()
    val daoPedidosArticulos = PedidoArticuloDao()
    val daoArticulos = ArticuloDao()

    var list_pedidos: MutableList<Pedido> = arrayListOf()
    var list_pedidos_con_mesa: MutableList<Pedido> = arrayListOf()

    private var client = OkHttpClient()
    private val context= VlatamRestoApp.instance.getContext()


    fun getListPedidos(){
        pedidos.postValue(Resource.loading(null))
        getCabeceras()
    }


    fun getCabeceras(){
        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_t?filter[en_edt]=0&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
        Log.e("TAG", "ENDPOINT $endpoint")

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                Log.e("TAG", "FAILED AL HACER GET DE CABECERAS ")
            }

            override fun onResponse(call: Call, response_facapa: Response) {
                var response: String =response_facapa.body()!!.string()


                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de familias ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")

                } else{


                    try {
                        var responceCabecera = Gson().fromJson(response, ResponseCabecera::class.java)

                        if(responceCabecera.count>=1){

                            for(cabecera in responceCabecera.fac_apa_t){

                                var pedido= Pedido()
                                pedido.mesa=0
                                pedido.hora=cabecera.hor
                                pedido.status="Sincronizado" // enviado y pendiente
                                pedido.total=cabecera.tot
                                pedido.comensales=0
                                pedido.id_cliente=cabecera.clt
                                pedido.is_sincronizado= true
                                pedido.id_sincronizado= cabecera.id

                                list_pedidos.add(pedido)
                            }

                            pedidos.postValue(Resource.success(list_pedidos))
                            getPedidosConMesa()

                        }else{
                            pedidos.postValue(Resource.error("Lista de pedidos vacia", list_pedidos))
                            getPedidosConMesa()

                        }
                        Log.e("TAG", "RESPONSE CABECERA "+ response)


                    } catch (e: Exception) {
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), null))
                        Log.e("TAG", "error al leer cabeceras "+e.message)
                    }

                }

            }

        })
    }

    private lateinit var responceLineasFactura: ResponseLineaFactura

    private fun getPedidosConMesa(){

        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?filter[en_edt]=0&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
        Log.e("TAG", "ENDPOINT EN GETPEDIDOS CON MESA $endpoint")

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "ON FAILED AL HACER GET DE PEDIDOS CON MESA ")
                pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
            }

            override fun onResponse(call: Call, response_ok: Response) {

                var response: String =response_ok.body()!!.string()

                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de familias ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")

                } else{

                    Log.e("TAG", "RESPONSE GET PEDIDOS CON MESA $response")

                    try {
                        responceLineasFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                        if(responceLineasFactura.count>=1) {
                            obtenerPedidosDesdeMesa()

                        }else{
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api), null))
                            Log.e("TAG", "error al leer enviarLineaFacturaConMesa ")
                        }


                    } catch (e: Exception) {
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                        Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                    }

                }
            }

        })



    }


    private fun obtenerPedidosDesdeMesa() {


        var url= sesion.getBasrUrl()+"v1/mes_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
        Log.e("TAG", "obtener pedido desde mesa $url")

        val request_2 = Request.Builder()
                .url(url)
                .build()

        client.newCall(request_2).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
            }

            override fun onResponse(call: Call, response_2: Response) {
                var response_mesa_t: String =response_2.body()!!.string()


                if (response_mesa_t.isEmpty() || response_mesa_t == "{}") {
                    Log.e("TAG", "error al leer lista de mesas ")

                } else if(response_mesa_t == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")
                } else{

                    try {
                        var data = Gson().fromJson(response_mesa_t, ResponseMesa::class.java)
                        Log.e("TAG", "response obtener pedido desde mesa $response_mesa_t")

                        var list_facaplint= ArrayList<FacApaLinT>()
                        var total=0.0

                        for(mesa in data.mes_t){
                            list_facaplint.clear()
                            total=0.0

                            if(mesa.est!=0){// esta ocupada
                                for(linea in responceLineasFactura.fac_apa_lin_t){
                                    if(linea.mes_t==mesa.id){
                                        list_facaplint.add(linea)
                                        //total+=(linea.can* linea.pre)
                                        total+=linea.imp
                                    }
                                }


                                Log.e("TAG", "TOTAL DE ESTE PEDIDO $total")
                                if(list_facaplint.size>0){
                                    var pedido= Pedido()
                                    pedido.mesa=mesa.num
                                    pedido.salon=mesa.sal
                                    pedido.id_mesa= mesa.id
                                    pedido.hora=""
                                    pedido.status="Sincronizado" // enviado y pendiente
                                    pedido.total=total
                                    pedido.comensales=list_facaplint[0].can_com
                                    pedido.id_cliente=0
                                    pedido.is_sincronizado= true
                                    pedido.id_sincronizado= 0

                                    list_pedidos_con_mesa.add(pedido)
                                }
                            }

                        }

                        pedidos_con_mesa.postValue(Resource.success(list_pedidos_con_mesa))

                    } catch (e: Exception) {
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                        Log.e("TAG", "error al leer mesas "+e.message)
                    }

                }

            }

        })



    }

    fun editarPedido(pedido: Pedido) {
        pedidos.postValue(Resource.loading(null))
        camiarStatusPedido(pedido, true)
    }


    fun camiarStatusPedido(pedido: Pedido, status: Boolean) {

        var param = JSONObject()

        param.put("en_edt", status)
        param.put("api_key_edt", sesion.getLoginInfo()!!.api_key )
        param.put("dep_tpv_edt ", sesion.getLoginInfo()!!.api_key_w[0].usr)


        if(pedido.mesa==0){// quiere decir que es un pedido sin mesa

            //http://149.56.103.187/API/vLatamERP_db_dat/v1/fac_apa_lin_t?filter[fac_apa]=9&api_key=api123
            var endpoint= sesion.getBasrUrl()+"v1/fac_apa_t/"+pedido.id_sincronizado+"?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
            //Log.e("TAG", "cambiar status ")
            Log.e("TAG", "cambiar status pedido sin mesa $endpoint")

            var JSON= MediaType.parse("application/json; charset=utf-8")

            var body = RequestBody.create(JSON, param.toString())

            var request = Request.Builder()
                    .url(endpoint)
                    .post(body)
                    .build()


            client.newCall(request).enqueue(object: Callback {

                override fun onFailure(call: Call, e: IOException) {
                    pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                    Log.e("TAG", "error  "+e.message)
                }

                override fun onResponse(call: Call, response_fac_apa: Response) {
                    var response: String =response_fac_apa.body()!!.string()

                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else {

                        Log.e("TAG", "RESPONSE cambiar status pedido $response")

                        try {

                            var resp: ResponseCabecera = Gson().fromJson(response, ResponseCabecera::class.java)

                            if (resp.count >= 1) {
                                if(status) {
                                    pedido.status = "En edición"
                                    //daoPedidos.add(pedido)
                                    getArticulos(pedido)
                                    //_pedido.postValue(Resource.error("finish", null))

                                    //getFamiliasLocales()
                                    //sendEvent(MainEvent.cerrarEditarPedido)

                                }else {
                                    pedido.status = "Sincronizado"
                                    var p= daoPedidos.queryForIdS(pedido.id_sincronizado)
                                    if(p!=null)
                                        daoPedidos.delete(p)

                                    Log.e("TAG", "ELIMINADO UN PEDIDO ${daoPedidos.queryForAllSincronizados(true).size}")
                                    _pedido.postValue(Resource.success(pedido))
                                }


                            } else {
                                pedidos.postValue(Resource.error(context.getString(R.string.error_api), null))
                            }
                            Log.e("TAG", "RESPONSE ENVIAR CABECERA " + response)


                        }catch (e: Exception){
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                        }
                    }



                }

            })

        }else{

            var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?filter[mes_t]="+pedido.id_mesa+"&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
            Log.e("TAG", "consulta articulos de la mesa $endpoint")

            val request = Request.Builder()
                    .url(endpoint)
                    .build()

            client.newCall(request).enqueue(object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                    Log.e("TAG", "error  "+e.message)
                }

                override fun onResponse(call: Call, response_fac: Response) {
                    var response: String =response_fac.body()!!.string()


                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{

                        Log.e("TAG", "RESPONSE obtener articulos de una mesa "+ response)

                        try {
                            var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                            if(data.count>=1) {
                                cambiarStatusFacLinT( data.fac_apa_lin_t, pedido, status)

                            }else{

                                Log.e("TAG", "showListArticulo count es 0 ")
                            }

                        } catch (e: Exception) {
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                            Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                        }

                    }

                }

            })

        }

    }

    private fun cambiarStatusFacLinT(list: List<FacApaLinT>, pedido: Pedido, status: Boolean){

        var param = JSONObject()

        param.put("en_edt", status)
        param.put("api_key_edt", sesion.getLoginInfo()!!.api_key )
        param.put("dep_tpv_edt ", sesion.getLoginInfo()!!.api_key_w[0].usr)

        var cont=0

        for(linea in list){
            var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t/"+linea.id+"?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")


            Log.e("TAG", "cambiar status  $endpoint")

            var JSON= MediaType.parse("application/json; charset=utf-8")

            var body = RequestBody.create(JSON, param.toString())

            var request = Request.Builder()
                    .url(endpoint)
                    .post(body)
                    .build()


            client.newCall(request).enqueue(object: Callback {

                override fun onFailure(call: Call, e: IOException) {
                    pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                    Log.e("TAG", "error  "+e.message)
                }

                override fun onResponse(call: Call, response_fac_apa: Response) {
                    var response: String =response_fac_apa.body()!!.string()

                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{

                        Log.e("TAG", "RESPONSE cambiar status pedido"+ response)

                        try {
                            var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                            if(data.count>=1) {
                                cont++
                                if(cont== list.size){
                                    if(status) {
                                        pedido.status = "En edición"
                                        //daoPedidos.add(pedido)
                                        getArticulos(pedido)
                                        //_pedido.postValue(Resource.error("finish", null))

                                    }else {
                                        pedido.status = "Sincronizado"
                                        var p= daoPedidos.queryForMesa(pedido.id_mesa!!)
                                        if(p!=null)
                                            daoPedidos.delete(p)

                                        Log.e("TAG", "ELIMINADO UN PEDIDO ${daoPedidos.queryForAllSincronizados(true).size}")
                                        _pedido.postValue(Resource.success(pedido))

                                    }

                                }


                            }else{
                                pedidos.postValue(Resource.error(context.getString(R.string.error_api), null))
                            }

                        } catch (e: Exception) {
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                        }
                    }
                }
            })

        }




    }


    fun getArticulos(pedidoSelected: Pedido){
        Log.e("TAG", "OBTENIENDO LOAS ARTICULOS ")

        //var pedidoSelected= daoPedidos.queryForIdS(p.id_sincronizado)

        if(pedidoSelected.mesa==0){// quiere decir que es un pedido sin mesa

            //http://149.56.103.187/API/vLatamERP_db_dat/v1/fac_apa_lin_t?filter[fac_apa]=9&api_key=api123
            var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?filter[fac_apa]="+pedidoSelected.id_sincronizado+"&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
            Log.e("TAG", "consulta articulos de fac_apa  $endpoint")

            val request = Request.Builder()
                    .url(endpoint)
                    .build()


            client.newCall(request).enqueue(object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                    Log.e("TAG", "error  "+e.message)
                }

                override fun onResponse(call: Call, response_fac_apa: Response) {
                    var response: String =response_fac_apa.body()!!.string()


                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{

                        Log.e("TAG", "RESPONSE obtener articulos de fac_apa "+response)

                        try {
                            var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)


                            if(data.count>=1) {
                                daoPedidos.add(pedidoSelected)
                                var ped= daoPedidos.queryForIdS(pedidoSelected.id_sincronizado)
                                Log.e("TAG", "ESTE ES EL PEDIDO NUEVO ALAMACENADO $ped")

                                for(linea in data.fac_apa_lin_t){
                                    val art=  daoArticulos.queryForId(linea.id_art).queryForFirst()

                                    var pedido = PedidoArticulo()
                                    pedido.id_articulo = art.id!!
                                    pedido.id_pedido = ped.id!!
                                    pedido.cantidad = linea.can
                                    pedido.descipcion_adicional = ""
                                    pedido.isEditable=false
                                    daoPedidosArticulos.add(pedido)

                                    //selectArticulos.add(ArticuloRow(0, art, linea.can, linea.obs, false))

                                }


                            }else{
                                Log.e("TAG", "showListArticulo count es 0 ")
                            }
                            _pedido.postValue(Resource.error("finish", null))


                        } catch (e: Exception) {
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                            Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                        }

                    }


                }

            })

        }else{

            //http://149.56.103.187/API/vLatamERP_db_dat/v1/fac_apa_lin_t?filter[mes_t]=ID_MESA&api_key=api123
            var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?filter[mes_t]="+pedidoSelected.id_mesa+"&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
            Log.e("TAG", "consulta articulos de la mesa $endpoint")

            val request = Request.Builder()
                    .url(endpoint)
                    .build()

            client.newCall(request).enqueue(object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                    Log.e("TAG", "error  "+e.message)
                }

                override fun onResponse(call: Call, response_fac: Response) {
                    var response: String =response_fac.body()!!.string()


                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{

                        Log.e("TAG", "RESPONSE obtener articulos de una mesa "+ response)

                        try {
                            var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)


                            if(data.count>=1) {
                                daoPedidos.add(pedidoSelected)
                                var ped= daoPedidos.queryForIdS(pedidoSelected.id_sincronizado)

                                for(linea in data.fac_apa_lin_t){

                                    val art=  daoArticulos.queryForId(linea.id_art).queryForFirst()

                                    var pedido = PedidoArticulo()
                                    pedido.id_articulo = art.id!!
                                    pedido.id_pedido = ped.id!!
                                    pedido.cantidad = linea.can
                                    pedido.descipcion_adicional = ""
                                    pedido.isEditable=false
                                    daoPedidosArticulos.add(pedido)

                                }

                            }else{

                                Log.e("TAG", "showListArticulo count es 0 ")
                            }
                            _pedido.postValue(Resource.error("finish", null))

                        } catch (e: Exception) {
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", null))
                            Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                        }

                    }

                }

            })

        }
    }


}



val modulePedidos= module{
    viewModel{ PedidosViewModel(get()) }

}