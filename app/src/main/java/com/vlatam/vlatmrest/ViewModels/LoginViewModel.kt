package com.vlatam.vlatmrest.ViewModels

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.preference.PreferenceManager
import android.util.Log
import com.google.gson.Gson
import com.vlatam.vlatmrest.Helper.Utils
import com.vlatam.vlatmrest.Models.DataLogin
import com.vlatam.vlatmrest.Models.ResponseDepT
import com.vlatam.vlatmrest.Models.ResponseEmpM
import com.vlatam.vlatmrest.Models.ResponseEntM
import com.vlatam.vlatmrest.R
import com.vlatam.vlatmrest.Servicios.PrefrencesManager
import com.vlatam.vlatmrest.VlatamRestoApp
import okhttp3.*
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import java.io.IOException
import java.lang.Exception

class LoginViewModel constructor(val preferences: PrefrencesManager) : ViewModel() {
    var message= MutableLiveData<String>()
    private val client= OkHttpClient()
    var user= MutableLiveData<HashMap<String, String>>()

    @SuppressLint("StaticFieldLeak")
    private val context= VlatamRestoApp.instance.getContext()

    private var path: String=""
    private var apikey: String=""
    private var filter: String=""



    fun login(filter_2: String, apikey_2: String, path_2: String){
        this.path= path_2
        this.apikey= apikey_2
        this.filter= filter_2

        if(Utils.isNetDisponible(context)){

            var basePath: String

            if (path.get(path.length - 1) != '/')
                basePath= "$path/API/vLatamERP_db_dat/"
            else
                basePath= path+"API/vLatamERP_db_dat/"

            //val service = ServiceVolley()
            //val apiController = APIController(service, basePath)

            loginFuncion(basePath)

        }else
            message.postValue(context.getString(R.string.error_internet))


    }

    private fun loginFuncion(basePath: String) {

        var endpoint= basePath+"v1/api_key_w?filter%5Bapikey%5D=$filter&api_key=$apikey"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL login $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                message.postValue(context.getString(R.string.error_api)+" ${e.message}")
            }

            override fun onResponse(call: Call, response_ok: Response) {

                Log.e("TAG", "URL ${response_ok.code()}")
                if(response_ok.code()==404){
                    modificaUrl()
                    return
                }


                var response: String =response_ok.body()!!.string()
                Log.e("TAG", "response login  $response")

                if(response.contains("El API Key de la solicitud no es válido", false)){
                    message.postValue("El API Key de la solicitud no es válido")
                    return
                }

                if (response.isEmpty() || response == "{}") {
                    message.postValue(context.getString(R.string.error_api))
                } else {
                    try {
                        var data = Gson().fromJson(response, DataLogin::class.java)
                        if(data.api_key == apikey) {

                            preferences.setLoginInfo(data)
                            preferences.setBaseUrl(basePath)
                            consumeDepT(data.api_key_w[0].usr)
                            //sendEvent(LoginEvent.onLoginSuccess)
                        }
                    } catch (e: Exception) {
                        message.postValue(context.getString(R.string.error_api))
                    }
                }





                Log.e("TAG", "RSULTADO LOGIN $response")
            }

        })

    }


    private fun modificaUrl() {


        var basePath: String

        if (path.get(path.length - 1) != '/')
            basePath= "$path/vLatamERP_db_dat/"
        else
            basePath= path+"vLatamERP_db_dat/"

        Log.e("TAG", "base path $basePath")

        loginFuncion(basePath)
    }

    private fun consumeDepT(usr: Int) {

        //val service = ServiceVolley()
        //val apiController = APIController(service, preferences.getBasrUrl())

        var endpoint= preferences.getBasrUrl()+"v1/dep_t/$usr?api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                message.postValue(context.getString(R.string.error_api)+" ${e.message}")
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()

                if (response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}") {
                    message.postValue("El API Key de la solicitud no es válido")
                } else
                    if (response.isEmpty() || response == "{}") {
                        message.postValue(context.getString(R.string.error_api))
                    } else {

                        try {
                            var data = Gson().fromJson(response, ResponseDepT::class.java)
                            if (data.dep_t[0] != null) {
                                preferences.setEmp(data.dep_t[0].emp)
                                preferences.setTrm(data.dep_t[0].trm_tpv.toString())
                                //sendEvent(LoginEvent.onLoginSuccess)
                                consumeEmp(data.dep_t[0].emp_div)
                            } else {
                                message.postValue(context.getString(R.string.error_api))

                            }

                            Log.e("TAG", "RSULTADO DEPT $response")

                        } catch (e: Exception) {
                            message.postValue(context.getString(R.string.error_api))
                        }

                    }
            }
        })




    }



    private fun consumeEmp(EMP_DIV: String) {

        //val service = ServiceVolley()
        //val apiController = APIController(service, preferences.getBasrUrl())

        var endpoint= preferences.getBasrUrl()+"v1/emp_m/$EMP_DIV?api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                message.postValue(context.getString(R.string.error_api)+" ${e.message}")

            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    message.postValue("El API Key de la solicitud no es válido")
                }else
                    if (response.isEmpty() || response == "{}") {
                        message.postValue(context.getString(R.string.error_api))
                    } else {

                        try {
                            var data : ResponseEmpM = Gson().fromJson(response, ResponseEmpM::class.java)
                            if(data.count==1) {
                                preferences.setEmpM(data.emp_m[0])
                                consumeEnt_m(data.emp_m[0].ent)
                            }else{
                                message.postValue(context.getString(R.string.error_api))
                            }

                            Log.e("TAG", "RSULTADO EMPM $response")

                        } catch (e: Exception) {
                            message.postValue(context.getString(R.string.error_api))
                        }

                    }




            }

        })

    }

    private fun consumeEnt_m(ent: Int) {

        var endpoint= preferences.getBasrUrl()+"v1/ent_m/$ent?api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL ENTM $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                message.postValue(context.getString(R.string.error_api)+" ${e.message}")

            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    message.postValue("El API Key de la solicitud no es válido")
                }else
                    if (response.isEmpty() || response == "{}") {
                        message.postValue(context.getString(R.string.error_api))
                    } else {

                        try {
                            var data : ResponseEntM = Gson().fromJson(response, ResponseEntM::class.java)
                            if(data.count==1) {
                                preferences.setEntM(data.ent_m[0])
                                preferences.setIsLogged()

                                var mMap = HashMap<String, String>()
                                mMap["nom_com"] = data.ent_m[0].nom_com
                                mMap["img"] = data.ent_m[0].img

                                user.postValue(mMap)
                            }else{
                                message.postValue(context.getString(R.string.error_api))
                            }

                            Log.e("TAG", "RSULTADO ENTM $response")

                        } catch (e: Exception) {
                            message.postValue(context.getString(R.string.error_api))
                        }

                    }


            }
        })
    }




}


val moduleLogin= module{
    viewModel{ LoginViewModel(get()) }
    single { PrefrencesManager(get()) }
    single { PreferenceManager.getDefaultSharedPreferences(VlatamRestoApp.instance.getContext()) }
}