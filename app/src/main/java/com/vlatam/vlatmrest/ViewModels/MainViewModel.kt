package com.vlatam.vlatmrest.ViewModels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.vlatam.vlatmrest.Helper.Resource
import com.vlatam.vlatmrest.Models.*
import com.vlatam.vlatmrest.R
import com.vlatam.vlatmrest.Servicios.PrefrencesManager
import com.vlatam.vlatmrest.VlatamRestoApp
import okhttp3.*
import org.json.JSONObject
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class MainViewModel constructor(val sesion: PrefrencesManager): ViewModel() {
    private val daoArticulos = ArticuloDao()
    private val daoPedidos = PedidoDao()
    private val daoCliente = ClienteDao()
    private val daoFamilias = FamiliaDao()
    private val daoMeasa = MesaDao()
    private val daoPedidosArticulos = PedidoArticuloDao()

    private var client = OkHttpClient()
    private val context= VlatamRestoApp.instance.getContext()

    var user_data= MutableLiveData<Resource<HashMap<String, String>>>()
    val pedidos = MutableLiveData<Resource<MutableList<Pedido>>> () /**  lista de pedidos del menu drawer **/
    val pedidos_sincronizados = MutableLiveData<Resource<MutableList<Pedido>>> () /**  lista de pedidos del menu drawer **/

    val familias = MutableLiveData<Resource<MutableList<Familia>>> ()
    val articulos = MutableLiveData<Resource<MutableList<Articulo>>> ()
    val articulos_pedido = MutableLiveData<Resource<MutableList<ArticuloRow>>> ()
    val pedido= MutableLiveData<Resource<Pedido>> ()
    val mesas = MutableLiveData<Resource<MesaCliente>> ()

    //private lateinit var mesa_aux: Mesa
    lateinit var pedidoSelected : Pedido
    private lateinit var response_linea_factura: ResponseLineaFactura

    fun checkDatosInicio(){

        if(sesion.isLogged()){
            val mMap = HashMap<String, String>()
            mMap["nom_com"] = sesion.getEntM().nom_com
            mMap["img"] = sesion.getEntM().img

            user_data.postValue(Resource.success(mMap))

        }else{
            user_data.postValue(Resource.error("",null))
            return
        }

        val tabaleArticulo = daoArticulos.queryForAll()
        if (tabaleArticulo.size == 0) {
            Log.e("TAG", "no hay articulos locales")
            // consultar de la api las tablas basicas y alacenar en local
            getTablasPrincipales()


        }else{
            getPedidos()
        }

        enviarToken()

    }


    fun getTablasPrincipales() {
        pedidos.value= Resource.loading(null)
        getListFamilias()
    }

    fun getPedidos() {
        var p = daoPedidos.queryForAllSincronizados(false)
        if(p.isEmpty()){
            nuevoPedido()
        }else
            pedidos.postValue(Resource.success(p))

        pedidos_sincronizados.postValue( Resource.success(daoPedidos.queryForAllSincronizados(true)))
    }

    fun nuevoPedido() {
        var date= SimpleDateFormat("dd/MM/yy HH:mm:ss")
        var pedido= Pedido()
        pedido.mesa=0
        pedido.hora=date.format(Date())
        pedido.status="Pendiente" // enviado y pendiente
        pedido.total=0.0
        pedido.comensales=0
        pedido.id_cliente=0
        pedido.is_sincronizado= false

        daoPedidos.add(pedido)
        getPedidos()
    }


    fun mostrarFamilias() {
        familias.value= Resource.success(daoFamilias.queryForAll())
    }

    fun selectedPedido(pedido: Pedido) {
        articulos_pedido.value= Resource.loading(null)

        pedidoSelected= pedido
        Log.e("TAG", "pedido seleccionado $pedido")
        //listArticulosPedido()
        var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()
        var selectArticulos = ArrayList<ArticuloRow>()

        for(articulo in listArticulos){
            val art=  daoArticulos.queryForId(articulo.id_articulo).queryForFirst()
            if(art!=null)
                selectArticulos.add(ArticuloRow(articulo.id!!, art, articulo.cantidad, articulo.descipcion_adicional, articulo.isEditable))
        }


        articulos_pedido.value= Resource.success(selectArticulos)

    }

    fun onSearchArticulo(texto: String) {
        var query_articulo= ArrayList<Articulo>()
        var art= daoArticulos.queryForAll()

        for (a in art) {
            if(a.name.toLowerCase().contains(texto.toLowerCase()))
                query_articulo.add(a)
        }

        articulos.value = Resource.success(query_articulo)

    }

    fun selectedFamilia(familia: Familia) {
        articulos.value= Resource.success(daoArticulos.queryForAllFamily(familia.id!!).query())
    }


    fun addArticulo(articulorow: ArticuloRow) {

        if (pedidoSelected == null) {
            Log.e("TAG", "PEDIDO SELECTED NULL")
            return
        }

        var pedido = PedidoArticulo()
        pedido.id_articulo = articulorow.articulo.id!!
        pedido.id_pedido = pedidoSelected.id!!
        pedido.cantidad = articulorow.cant
        pedido.descipcion_adicional = articulorow.descripcion
        daoPedidosArticulos.add(pedido)
        calculateTotalPedido()


        //if (!pedidoSelected.is_sincronizado) {


    }

    fun calculateTotalPedido() {

        var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()
        var total = 0.0

        for(articulo in listArticulos){
            val art=  daoArticulos.queryForId(articulo.id_articulo).queryForFirst()
            if(art!=null){
                total+= art.pvp*articulo.cantidad
            }
        }
        pedidoSelected.total= total
        daoPedidos.update(pedidoSelected)

        pedido.value= Resource.success(pedidoSelected)
    }


    fun deleteArticulo(articuloRow: ArticuloRow) {
        daoPedidosArticulos.delete( daoPedidosArticulos.queryForId(articuloRow.id))
        calculateTotalPedido()
        selectedPedido(pedidoSelected)

    }

    fun updateArticulo(articuloRow: ArticuloRow) {
        var pedido_articulo= daoPedidosArticulos.queryForId(articuloRow.id)
        pedido_articulo.cantidad= articuloRow.cant
        pedido_articulo.descipcion_adicional= articuloRow.descripcion

        daoPedidosArticulos.update( pedido_articulo)

        calculateTotalPedido()
        selectedPedido(pedidoSelected)
    }



    fun crearPedido(result: String) {

        var pedidos = result.split("#")

        var date = SimpleDateFormat("dd/MM/yy HH:mm:ss").format(Date())
        var pedido = Pedido()
        pedido.mesa = 0
        pedido.hora = date
        pedido.status = "Qr"
        pedido.total = 0.0
        pedido.comensales = 0
        pedido.id_cliente = 0
        pedido.is_sincronizado = false

        daoPedidos.add(pedido)

        var nuevo_pedido= daoPedidos.queryForDate(date)
        var list_articulos= arrayListOf<ArticuloRow>()
        var total = 0.0


        pedidos.forEach {
            if(it.isNotEmpty()){
                var articulos = it.split(";")
                val art = daoArticulos.queryForId(articulos[0].toInt()).queryForFirst()
                if (art != null) {
                    var cant= articulos[1].toInt()
                    list_articulos.add(ArticuloRow(art.id!!, art, cant,"", false))
                    total+= art.pvp*cant

                    var pedido = PedidoArticulo()
                    pedido.id_articulo = art.id!!
                    pedido.id_pedido = nuevo_pedido.id!!
                    pedido.cantidad = cant
                    daoPedidosArticulos.add(pedido)
                }
            }

        }

        nuevo_pedido.total= total
        daoPedidos.update(nuevo_pedido)

        getPedidos()

    }




    fun sincronizar() {

        mesas.value= Resource.loading(null)

        if(pedidoSelected.is_sincronizado){
            Log.e("TAG", "pedido ya ha sido sincronizado $pedidoSelected")
            sincronizarEnviados()
            return
        }

        getSalon()
    }

    private fun sincronizarEnviados() {
        var control= true
        if(pedidoSelected.mesa==0){//sin mesa

            var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()


            for(ped_articulo in listArticulos){

                if(ped_articulo.isEditable){
                    control=false
                    Log.e("TAG", "este articulo es editable sin mesa $ped_articulo")
                    val art=  daoArticulos.queryForId(ped_articulo.id_articulo).queryForFirst()
                    enviarLineaFactura( ped_articulo, art, pedidoSelected.id_sincronizado, true)
                }
            }


            if(control){
                mesas.value= Resource.error("No hay articulos nuevo que enviar, cerrando pedido", null)
                mesas.value= Resource.loading(null)
                comprobarEnvioCerrarPedido()
            }

        }else{
            //ya el pedido esta abierto, solo enviar las lineas que son editables
            var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()
            //mesa_aux= daoMeasa.queryForId(pedidoSelected.id_mesa!!)

            for(ped_articulo in listArticulos){
                if(ped_articulo.isEditable){
                    control=false
                    Log.e("TAG", "este articulo es editable CON mesa $ped_articulo")
                    val art=  daoArticulos.queryForId(ped_articulo.id_articulo).queryForFirst()
                    agregarArticuloConMesa( ped_articulo, art )
                }

            }


            if(control){
                mesas.value= Resource.error("No hay articulos nuevo que enviar, cerrando pedido", null)
                mesas.value= Resource.loading(null)
                comprobarEnvioCerrarPedido()
            }


        }




    }




    fun enviar(des: String, sin_mesa: Boolean, sin_cliente: Boolean, mesa: Mesa, comensales: Int, cliente: Cliente) {
        articulos_pedido.value= Resource.loading(null)


        if(sin_mesa){
            Log.e("TAG", "envio sin mesa")
            enviarCabecera(des, cliente.id!!)
        }else{
            pedidoSelected.id_mesa= mesa.id!!
            pedidoSelected.mesa= mesa.num

            //mesa_aux= daoMeasa.queryForId(mesa.id)

            pedidoSelected.comensales= comensales

            //Log.e("TAG", "envio con mesa $mesa_aux")
            var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()

            /*
            0 => Libre => Verde
            1=> Ocuapda => Rojo
            2=> Reservada => Naranja
             */

            when(mesa.est){
                0->{
                    for(ped_articulo in listArticulos){
                        val art=  daoArticulos.queryForId(ped_articulo.id_articulo).queryForFirst()
                        if(art!=null){
                            enviarLineaFacturaConMesa(mesa, comensales, ped_articulo, art)
                        }
                    }
                }

                1->{
                    cambiarStatusPedido(true)

                    /*for(ped_articulo in listArticulos){
                        val art=  daoArticulos.queryForId(ped_articulo.id_articulo).queryForFirst()

                        agregarArticuloConMesa(mesa, comensales, ped_articulo, art )

                    }*/
                }
                2->{

                }

            }

        }

    }



    fun cambiarStatusPedido(status: Boolean) {


        //obtengo todas las lineas del pedido y les cambio el estatus
        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?filter[mes_t]="+pedidoSelected.id_mesa+"&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
        Log.e("TAG", "consulta articulos de la mesa $endpoint")

        val request = Request.Builder()
                .url(endpoint)
                .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                articulos_pedido.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                Log.e("TAG", "error  "+e.message)
            }

            override fun onResponse(call: Call, response_fac: Response) {
                var response: String =response_fac.body()!!.string()


                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de familias ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")

                } else{

                    Log.e("TAG", "obtener articulos de una mesa "+ response)

                    try {
                        response_linea_factura  = Gson().fromJson(response, ResponseLineaFactura::class.java)

                        if(response_linea_factura.count>=1) {
                            cambiarStatusFacLinT( response_linea_factura.fac_apa_lin_t, status)

                        }else{

                            Log.e("TAG", "showListArticulo count es 0 ")
                        }

                    } catch (e: Exception) {
                        articulos_pedido.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                        Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                    }

                }

            }

        })



    }

    private fun cambiarStatusFacLinT(list: List<FacApaLinT>, status: Boolean){
        //cambio el status de cada linea
        var param = JSONObject()

        param.put("en_edt", status)
        param.put("api_key_edt", sesion.getLoginInfo()!!.api_key )
        param.put("dep_tpv_edt ", sesion.getLoginInfo()!!.api_key_w[0].usr)

        var cont=0

        for(linea in list){
            var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t/"+linea.id+"?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")


            Log.e("TAG", "cambiar status  $endpoint")

            var JSON= MediaType.parse("application/json; charset=utf-8")

            var body = RequestBody.create(JSON, param.toString())

            var request = Request.Builder()
                    .url(endpoint)
                    .post(body)
                    .build()


            client.newCall(request).enqueue(object: Callback {

                override fun onFailure(call: Call, e: IOException) {
                    articulos_pedido.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                    Log.e("TAG", "error  "+e.message)
                }

                override fun onResponse(call: Call, response_fac_apa: Response) {
                    var response: String =response_fac_apa.body()!!.string()

                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{

                        Log.e("TAG", "cambiar status pedido cambiarStatusFacLinT"+ response)

                        try {
                            var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                            if(data.count>=1) {
                                cont++
                                if(cont== list.size){
                                    if(status){ // si lo estoy abriendo envio todas las lineas
                                        var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()

                                        for(ped_articulo in listArticulos){
                                            val art=  daoArticulos.queryForId(ped_articulo.id_articulo).queryForFirst()

                                            agregarArticuloConMesa( ped_articulo, art )

                                        }
                                    }else{// si los estoy cerrando actualizo la vista

                                        comprobarEnvio()
                                    }
                                }


                            }else{
                                articulos_pedido.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                                Log.e("TAG", "showListArticulo count es 0 ")
                            }

                        } catch (e: Exception) {
                            articulos_pedido.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                            Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                        }
                    }
                }
            })

        }




    }



    /**
     * SI ENVIAMOS CABECRA SIGNIFICA QUE ES UN PEDIDO SIN MESA
     */
    fun enviarCabecera(observacion: String, cliente: Int){
        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")


        var date= SimpleDateFormat("yyyy-MM-dd")
        var hora= SimpleDateFormat("HH:mm:ss")
        var param = JSONObject()
        param.put("emp", sesion.getLoginInfo()!!.api_key_w[0].id.toString())
        param.put("emp_div", sesion.getLoginInfo()!!.api_key_w[0].id.toString())
        param.put("fch", date.format(Date())+"T"+hora.format(Date())+".000Z")
        param.put("hor", hora.format(Date()))
        param.put("clt", cliente) // si elegimos un cliente sino va 0
        param.put("obs", observacion)
        param.put("trm_tpv", sesion.getTrm().toInt())
        param.put("tot", pedidoSelected.total.toString())
        param.put("dep_tpv", sesion.getLoginInfo()!!.api_key_w[0].usr)
        param.put("mes_t", 0)
        param.put("api_key", sesion.getLoginInfo()?.api_key )
        param.put("ser", 0)
        param.put("por_dto", 0)
        param.put("alt_usr", sesion.getLoginInfo()!!.api_key_w[0].usr)
        param.put("trm_ptr", "")
        param.put("txt", "${sesion.getLoginInfo()!!.api_key_w[0].name} / sin mesa")
        param.put("nro_tkt", 0)

        Log.e("TAG", "json en cabecera de factura "+param.toString())


        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {

                articulos_pedido.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                Log.e("TAG", "error "+e.message)
            }

            override fun onResponse(call: Call, response_fac: Response) {
                var response: String =response_fac.body()!!.string()


                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de familias ")
                }
                else
                    if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{


                        try {
                            var data : ResponseCabecera = Gson().fromJson(response, ResponseCabecera::class.java)

                            if(data.count==1){

                                var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()

                                for(ped_articulo in listArticulos){
                                    val art=  daoArticulos.queryForId(ped_articulo.id_articulo).queryForFirst()
                                    if(art!=null){
                                        enviarLineaFactura(ped_articulo, art, data.fac_apa_t[0].id, false)
                                    }
                                }

                            }else{
                                articulos_pedido.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                            }
                            Log.e("TAG", "RESPONSE ENVIAR CABECERA "+ response)


                        } catch (e: Exception) {
                            articulos_pedido.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                            Log.e("TAG", "error al leer familias "+e.message)
                        }

                    }


            }

        })

    }


    fun enviarLineaFactura(pedido_articulo: PedidoArticulo, articulo: Articulo, id_cabecera: Int, cerrar: Boolean) {
        //cerrar, para los pedidos sincronizados

        var param = JSONObject()
        param.put("fac_apa", id_cabecera)
        param.put("id_art", articulo.id)
        param.put("name", articulo.name)
        param.put("can ", pedido_articulo.cantidad)
        param.put("pre", articulo.pvp) // si elegimos un cliente sino va 0
        param.put("por_dto", 0)
        param.put("por_dto_2", 0)
        param.put("pre_net", articulo.pvp)
        param.put("imp",pedido_articulo.cantidad*articulo.pvp)
        when(articulo.reg_iva_vta){
            "G"->{
                param.put("por_iva", sesion.getEmpM().por_iva_gen)
            }
            "R"->{
                param.put("por_iva", sesion.getEmpM().por_iva_red)
            }
            "S"->{
                param.put("por_iva", sesion.getEmpM().por_iva_sup)
            }
            "E"->{
                param.put("por_iva", sesion.getEmpM().por_iva_esp)
            }
        }
        //param.put("por_iva", 0)
        /**por_iva => obtenidos de la empresa, cada articulo tiene definido un tipo de IVA y
         * en la empresa, estan definidos los porcentajes por cada tipo.*/
        param.put("mes_t", 0)
        param.put("pro", 0)
        param.put("art", articulo.id)
        param.put("imp_tpv", articulo.imp_tpv_t)
        param.put("can_imp", "")
        param.put("tie_ult_cmd", "")
        param.put("obs", pedido_articulo.descipcion_adicional)
        param.put("can_com", 1)
        param.put("dep_tpv", id_cabecera)
        param.put("api_key", sesion.getLoginInfo()?.api_key )

        Log.e("TAG", "json enviar linea de factura sin mesa "+param.toString())

        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")

        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                articulos_pedido.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                Log.e("TAG", "error  "+e.message)
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String =response_ok.body()!!.string()
                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de familias ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")

                } else{

                    Log.e("TAG", "RESPONSE ENVIAR LINEA DE FACTURA SIN MESA "+ response)

                    try {
                        var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                        if(data.count==1) {
                            pedido_articulo.isEditable=false
                            daoPedidosArticulos.update(pedido_articulo)
                            if(cerrar)
                                comprobarEnvioCerrarPedido()
                            else
                                comprobarEnvio()
                        }else{
                            articulos_pedido.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        }

                    } catch (e: Exception) {
                        articulos_pedido.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        Log.e("TAG", "error al leer familias "+e.message)
                    }

                }
            }

        })


    }

    fun enviarLineaFacturaConMesa(mesa: Mesa, comensales: Int, pedido_articulo: PedidoArticulo, articulo: Articulo) {


        var param = JSONObject()
        param.put("fac_apa", 0)
        param.put("id_art", articulo.id)
        param.put("name", articulo.name)
        param.put("can ", pedido_articulo.cantidad)
        param.put("pre", articulo.pvp) // si elegimos un cliente sino va 0
        param.put("por_dto", 0)
        param.put("por_dto_2", 0)
        param.put("pre_net", articulo.pvp)
        param.put("imp",pedido_articulo.cantidad*articulo.pvp)
        when(articulo.reg_iva_vta){
            "G"->{
                param.put("por_iva", sesion.getEmpM().por_iva_gen)
            }
            "R"->{
                param.put("por_iva", sesion.getEmpM().por_iva_red)
            }
            "S"->{
                param.put("por_iva", sesion.getEmpM().por_iva_sup)
            }
            "E"->{
                param.put("por_iva", sesion.getEmpM().por_iva_esp)
            }
        }
        /**por_iva => obtenidos de la empresa, cada articulo tiene definido un tipo de IVA y
         * en la empresa, estan definidos los porcentajes por cada tipo.*/
        param.put("mes_t", mesa.id)
        param.put("pro", 0)
        param.put("art", articulo.id)
        param.put("imp_tpv", articulo.imp_tpv_t)
        param.put("can_imp", "")
        param.put("tie_ult_cmd", "")
        param.put("obs", pedido_articulo.descipcion_adicional)
        param.put("can_com", comensales)
        param.put("dep_tpv", 0)
        param.put("api_key", sesion.getLoginInfo()?.api_key )

        Log.e("TAG", "parametros POST"+param.toString())

        var JSON= MediaType.parse("application/json; charset=utf-8")
        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")

        var body = RequestBody.create(JSON, param.toString())
        var client = OkHttpClient()

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()

        /**try {
        val response = client.newCall(request).execute()
        return response.body()!!.string()
        } catch (e: Exception) {
        e.printStackTrace()
        }*/

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: okhttp3.Call, e: IOException) {
                articulos_pedido.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                Log.e("TAG", "RESPOSE fallo  POST" + e.message)
            }

            override fun onResponse(call: okhttp3.Call, response_ok: Response) {
                var response: String =response_ok.body()!!.string()
                Log.e("TAG", "RESPOSE POST " + response)


                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de familias ")

                } else if (response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}") {
                    Log.e("TAG", "El API Key de la solicitud no es válido ")

                } else {

                    Log.e("TAG", "RESPONSE ENVIAR LINEA DE FACTURA CON MESA " + response)

                    try {
                        var data: ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                        if (data.count == 1) {
                            pedido_articulo.isEditable = false
                            daoPedidosArticulos.update(pedido_articulo)
                            comprobarEnvio()

                        } else {
                            articulos_pedido.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                            Log.e("TAG", "error al leer enviarLineaFacturaConMesa ")
                        }


                    } catch (e: Exception) {
                        articulos_pedido.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        Log.e("TAG", "error al leer enviarLineaFacturaConMesa " + e.message)
                    }

                }


            }
        })

    }

    private fun agregarArticuloConMesa( pedido_articulo: PedidoArticulo, articulo: Articulo) {
        // other.id_mesa== this.id_mesa
        var param = JSONObject()
        param.put("fac_apa", 0)
        param.put("id_art", articulo.id)
        param.put("name", articulo.name)
        param.put("can ", pedido_articulo.cantidad)
        param.put("pre", articulo.pvp) // si elegimos un cliente sino va 0
        param.put("por_dto", 0)
        param.put("por_dto_2", 0)
        param.put("pre_net", articulo.pvp)
        param.put("imp",articulo.pvp*pedido_articulo.cantidad)
        when(articulo.reg_iva_vta){
            "G"->{
                param.put("por_iva", sesion.getEmpM().por_iva_gen)
            }
            "R"->{
                param.put("por_iva", sesion.getEmpM().por_iva_red)
            }
            "S"->{
                param.put("por_iva", sesion.getEmpM().por_iva_sup)
            }
            "E"->{
                param.put("por_iva", sesion.getEmpM().por_iva_esp)
            }
        }
        /**por_iva => obtenidos de la empresa, cada articulo tiene definido un tipo de IVA y
         * en la empresa, estan definidos los porcentajes por cada tipo.*/
        param.put("mes_t", pedidoSelected.id_mesa)
        param.put("pro", 0)
        param.put("art", articulo.id)
        param.put("imp_tpv", articulo.imp_tpv_t)
        param.put("can_imp", "")
        param.put("tie_ult_cmd", "")
        param.put("obs", pedido_articulo.descipcion_adicional)
        param.put("can_com", pedidoSelected.comensales)
        param.put("dep_tpv", 0)
        param.put("api_key", sesion.getLoginInfo()?.api_key )


        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")



        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                articulos_pedido.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                Log.e("TAG", "error "+e.message)
            }

            override fun onResponse(call: Call, response_fac_apa_lin_t: Response) {
                var response: String =response_fac_apa_lin_t.body()!!.string()


                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de familias ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")

                } else{

                    Log.e("TAG", "RESPONSE ENVIAR LINEA DE FACTURA CON MESA "+ response)

                    try {
                        var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                        if (data.count == 1) {
                            pedido_articulo.isEditable = false
                            daoPedidosArticulos.update(pedido_articulo)
                            comprobarEnvioCerrarPedido()

                        } else {
                            articulos_pedido.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                            Log.e("TAG", "error al leer enviarLineaFacturaConMesa ")
                        }

                        //if(data.count==1){
                        //pedidoSelected.total+=data.fac_apa_lin_t[0].pre*data.fac_apa_lin_t[0].can

                        //if(pedidoSelected.status=="En edición"){
                        //  daoPedidos.update(pedidoSelected)
                        //}

                        //sendEvent(MainEvent.updatePedido, pedidoSelected)
                        //}
                        //listArticulosPedido()

                    } catch (e: Exception) {
                        articulos_pedido.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                    }

                }

            }

        })
    }

    private fun comprobarEnvio() {

        Log.e("TAG", "conprobar envio ")
        var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()

        var control= false

        for(ped_articulo in listArticulos){
            if(ped_articulo.isEditable){
                control=true
            }
        }

        if(!control) { // si todos los articulos fueron enviados
            /**cuando ya haya finalizado la sincronizacion */
            daoPedidos.delete(pedidoSelected)
            for (pedidoArticulo in daoPedidosArticulos.queryForAll(pedidoSelected.id!!)) {
                daoPedidosArticulos.delete(pedidoArticulo)
            }
            getPedidos()
            articulos_pedido.postValue(Resource.error("Pedido enviado exitosamente", null))


        }

    }

    private fun comprobarEnvioCerrarPedido() {
        Log.e("TAG", "conprobar envio $pedidoSelected")
        //Log.e("TAG", "conprobar envio $mesa_aux")
        var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()

        var control= false

        for(ped_articulo in listArticulos){
            if(ped_articulo.isEditable){
                control=true
            }
        }

        if(!control) { // si todos los articulos fueron enviados

            if(pedidoSelected.mesa==0){
                cambiarStatusPedidoSinMesa(false)
            } else{
                cambiarStatusPedido(false)
            }


            /*if (mesa_aux.est==1) { // si es un pedido abierto se debe cerrar
                if(response_linea_factura.count>=1) {
                    cambiarStatusFacLinT( response_linea_factura.fac_apa_lin_t, false)
                }
            }else*/

        }

    }

    fun cambiarStatusPedidoSinMesa(status: Boolean) {

        //http://149.56.103.187/API/vLatamERP_db_dat/v1/fac_apa_lin_t?filter[fac_apa]=9&api_key=api123
        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_t/"+pedidoSelected.id_sincronizado+"?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
        Log.e("TAG", "cambiar status cambiarStatusPedidoSinMesa  $endpoint")
        var param = JSONObject()

        param.put("en_edt", status)
        param.put("api_key_edt", sesion.getLoginInfo()!!.api_key )
        param.put("dep_tpv_edt ", sesion.getLoginInfo()!!.api_key_w[0].usr)

        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()


        client.newCall(request).enqueue(object: Callback {

            override fun onFailure(call: Call, e: IOException) {
                pedidos.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                Log.e("TAG", "error  "+e.message)
            }

            override fun onResponse(call: Call, response_fac_apa: Response) {
                var response: String =response_fac_apa.body()!!.string()

                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de familias ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")

                } else {

                    Log.e("TAG", "RESPONSE cambiar status pedido $response")

                    try {
                        var resp: ResponseCabecera = Gson().fromJson(response, ResponseCabecera::class.java)

                        if (resp.count >= 1) {
                            if(status) {

                            }else {
                                daoPedidos.delete(pedidoSelected)
                                for (pedidoArticulo in daoPedidosArticulos.queryForAll(pedidoSelected.id!!)) {
                                    daoPedidosArticulos.delete(pedidoArticulo)
                                }
                                getPedidos()
                                articulos_pedido.postValue(Resource.error("Pedido enviado exitosamente", null))
                            }



                        } else {
                            pedidos.postValue( Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        }

                    }catch (e: Exception){
                        pedidos.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                    }
                }



            }

        })

    }

    fun logout() {
        sesion.logout()
        daoPedidos.removeAll()
        daoArticulos.removeAll()
        daoCliente.removeAll()
        daoPedidosArticulos.removeAll()
        checkDatosInicio()
    }




    fun getListFamilias() {

        var endpoint= sesion.getBasrUrl()+"v1/fam_m?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")+"&filter[dis_app]=true"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "url familia $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                pedidos.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                Log.e("TAG", "error al leer familias "+e.message)
            }

            override fun onResponse(call: Call, response_fam: Response) {
                var response: String =response_fam.body()!!.string()

                Log.e("TAG", "response familia $response")

                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de familias ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")

                } else{

                    try {
                        var data = Gson().fromJson(response, ResponseFamilias::class.java)
                        if(!daoFamilias.queryForAll().isEmpty())
                            daoFamilias.removeAll()

                        for(familia in data.fam_m){
                            daoFamilias.add(familia)
                            //Log.e("TAG", "nuevo familia "+familia.toString())

                        }

                        getListClientes()

                    } catch (e: Exception) {
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        Log.e("TAG", "error al leer familias "+e.message)
                    }

                }
            }

        })

    }

    fun getListClientes() {

        var endpoint= sesion.getBasrUrl()+"v1/ent_m?filter[id_es_clt]=&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")


        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                Log.e("TAG", "error al leer clientes "+e.message)

            }

            override fun onResponse(call: Call, response_ent: Response) {
                var response: String =response_ent.body()!!.string()


                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de clientes ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")

                } else{

                    try {
                        var data = Gson().fromJson(response, ResponseClientes::class.java)
                        if(!daoCliente.queryForAll().isEmpty())
                            daoCliente.removeAll()

                        for(cliente in data.ent_m){
                            daoCliente.add(cliente)
                            Log.e("TAG", "nuevo cliente "+cliente.toString())
                        }

                        getListArticulos()


                    } catch (e: Exception) {
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        Log.e("TAG", "error al leer clientes "+e.message)

                    }

                }

            }


        })
    }
    fun getListArticulos() {

        var endpoint= sesion.getBasrUrl()+"v1/art_m?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")+"&filter[dis_app]=true"
        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "url articulo $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                Log.e("TAG", "error al leer articulos "+e.message)
            }

            override fun onResponse(call: Call, response_art: Response) {

                var response: String =response_art.body()!!.string()
                Log.e("TAG", "response articulo $response")

                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de articulos ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")
                } else{

                    try {
                        var data = Gson().fromJson(response, ResponseArticulos::class.java)
                        if(!daoArticulos.queryForAll().isEmpty())
                            daoArticulos.removeAll()

                        for(artculo in data.art_m){
                            if(artculo.tpv_vis!!) {
                                daoArticulos.add(artculo)
                                //Log.e("TAG", "nuevo articulo "+artculo.toString())
                            }
                        }

                        getMesas()



                    } catch (e: Exception) {
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        Log.e("TAG", "error al leer articulos "+e.message)

                    }

                }

            }

        })


    }

    private fun getMesas() {


        var endpoint= sesion.getBasrUrl()+"v1/mes_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                pedidos.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                Log.e("TAG", "error al leer mesas "+e.message)
            }

            override fun onResponse(call: Call, response_mesa_t: Response) {
                var response: String =response_mesa_t.body()!!.string()


                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de mesas ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")
                } else{
                    Log.e("TAG", "OBTENIENDO LA LISTA DE MESAS $response")

                    try {
                        var data = Gson().fromJson(response, ResponseMesa::class.java)
                        daoMeasa.removeAll()

                        for(mesa in data.mes_t){
                            daoMeasa.add(mesa)

                        }
                        getPedidos()

                    } catch (e: Exception) {
                        pedidos.postValue( Resource.error(" ${e.message}", arrayListOf()))
                        Log.e("TAG", "error al leer mesas "+e.message)
                    }

                }


            }



        })


    }

    private var responseSalT:  MutableList<SalT> = arrayListOf()

    private fun getSalon() {


        var endpoint= sesion.getBasrUrl()+"v1/sal_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                pedidos.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                Log.e("TAG", "error al leer mesas "+e.message)
            }

            override fun onResponse(call: Call, response_mesa_t: Response) {
                var response: String =response_mesa_t.body()!!.string()


                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de mesas ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")
                } else{
                    Log.e("TAG", "OBTENIENDO LA LISTA DE MESAS $response")

                    try {
                        var data = Gson().fromJson(response, ResponseSalT::class.java)

                        if(data.count>=1){
                            responseSalT= data.sal_t as MutableList<SalT>
                            showDialogoMesas()
                        }


                    } catch (e: Exception) {
                        pedidos.postValue( Resource.error(" ${e.message}", arrayListOf()))
                        Log.e("TAG", "error al leer mesas "+e.message)
                    }

                }


            }



        })


    }

    private fun showDialogoMesas(){
        var endpoint= sesion.getBasrUrl()+"v1/mes_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                pedidos.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
                Log.e("TAG", "error al leer mesas "+e.message)
            }

            override fun onResponse(call: Call, response_mesa_t: Response) {
                var response: String =response_mesa_t.body()!!.string()


                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de mesas ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")
                } else{
                    Log.e("TAG", "OBTENIENDO LA LISTA DE MESAS $response")

                    try {
                        var data = Gson().fromJson(response, ResponseMesa::class.java)
                        daoMeasa.removeAll()

                        var list_mesas= ArrayList<Mesa>()

                        for(mesa in data.mes_t){
                            daoMeasa.add(mesa)
                            mesa.name=getSalonName(mesa.sal!!)+" - Mesa ${mesa.num}"
                            list_mesas.add(mesa)
                        }

                        mesas.postValue(Resource.success(MesaCliente(list_mesas, daoCliente.queryForAll())))

                        //sendEventMesas(list_mesas, daoCliente.queryForAll())

                    } catch (e: Exception) {
                        pedidos.postValue( Resource.error(" ${e.message}", arrayListOf()))
                        Log.e("TAG", "error al leer mesas "+e.message)
                    }

                }


            }



        })

    }

    private fun getSalonName(id: Int): String {

        responseSalT.forEach {
            if(it.id==id){
                return it.name
            }
        }

        return ""
    }


    fun enviarToken(){
        val refreshedToken = FirebaseInstanceId.getInstance().token

        if(!sesion.isLogged()){
            return
        }

        if(refreshedToken!= null){

            var endpoint= sesion.getBasrUrl()+"/ACT_TOK"

            var param = JSONObject()

            param.put("token", refreshedToken)
            param.put("apikey", sesion.getLoginInfo()!!.api_key)
            param.put("proyecto", "vLatamResto" )

            var JSON= MediaType.parse("application/json; charset=utf-8")

            var body = RequestBody.create(JSON, param.toString())

            var request = Request.Builder()
                    .url(endpoint)
                    .post(body)
                    .build()



            client.newCall(request).enqueue(object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("TAG", "error al enviar token "+e.message)
                }

                override fun onResponse(call: Call, response_mesa_t: Response) {
                    var response: String =response_mesa_t.body()!!.string()
                    Log.e("TAG", "token enviado $response")

                }
            })
        }

    }


}


val moduleMain= module{
    viewModel{ MainViewModel(get()) }
    //single { PrefrencesManager(get()) }
//    single { PreferenceManager.getDefaultSharedPreferences(VlatamTiendaApplication.instance.getContext()) }
}