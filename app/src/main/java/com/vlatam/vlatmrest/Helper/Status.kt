package com.vlatam.vlatmrest.Helper

/**
 * @author carlosleonardocamilovargashuaman on 4/23/18.
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}