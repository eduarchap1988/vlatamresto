package com.vlatam.vlatmrest

import android.app.Application
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.vlatam.vlatmrest.ViewModels.moduleLogin
import com.vlatam.vlatmrest.ViewModels.moduleMain
import com.vlatam.vlatmrest.ViewModels.modulePedidos
import io.fabric.sdk.android.Fabric
import org.koin.android.ext.android.startKoin
import com.google.firebase.iid.FirebaseInstanceId
import uk.co.chrisjenx.calligraphy.CalligraphyConfig


class VlatamRestoApp : Application() {

    companion object {
        @get:Synchronized lateinit var instance: VlatamRestoApp
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance= this
        startKoin(this, listOf(moduleLogin, moduleMain, modulePedidos))
        Fabric.with(this, Crashlytics())


        val refreshedToken = FirebaseInstanceId.getInstance().token

        Log.e("TAG", "Refreshed token: $refreshedToken")

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/roboto.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(refreshedToken)
    }


    fun getContext() = applicationContext


}